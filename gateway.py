from event_process import EventProcess
import falcon
import json


class MGCloudGateway:
    def __init__(self):
        self.event_process = EventProcess()
        print('Ready')

    @staticmethod
    def decode(req):
        body = req.stream.read()
        body = body.decode('utf-8')
        request = json.loads(body)
        return request

    def on_post(self, req, resp):
        request = MGCloudGateway.decode(req)
        print(request.keys())
        # try:
        self.event_process.process(request)
        success, message = True, ''
        # except Exception as e:
        #     print(e)
        #     success, message = False, e

        resp.body = json.dumps({'success': True, 'message': ''})


app = falcon.API()
app.add_route('/push_events', MGCloudGateway())
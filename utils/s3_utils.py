import base64
import boto3

from config.config import bucket_name, region, folder, profile_name, aws_access_key_id, aws_secret_access_key


class S3:
    def __init__(self):
        # self.session = boto3.Session(profile_name=profile_name)
        self.session = boto3.Session(aws_access_key_id=aws_access_key_id,
                                     aws_secret_access_key=aws_secret_access_key)
        self.s3_resource = self.session.resource('s3')
        self.bucket = self.s3_resource.Bucket(bucket_name)

    @staticmethod
    def decode(image_string):
        byte_image_string = base64.b64decode(image_string)
        return byte_image_string

    def upload(self, image_string, uuid):
        self.byte_image_string = S3.decode(image_string)
        self.fname = f"alert_{uuid}.jpg"
        ret = self.bucket.put_object(Key=f'{folder}/{self.fname}', Body=self.byte_image_string,
                                     ContentType='image/jpeg')
        url = f"https://{bucket_name}.s3.{region}.amazonaws.com/{folder}/{self.fname}"
        return url

    def push(self, image_string, uuid):
        url = self.upload(image_string, uuid)
        print('Uploaded to s3')
        return url

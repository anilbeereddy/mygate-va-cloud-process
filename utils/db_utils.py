import psycopg2
import json

column_list = ['vsource', 'models', 'message', 'level', 'event', 'label', 'timestamp', 'image', 'society_id',
               'device_id', 'uuid']


class DB:
    def __init__(self):
        self.conn = psycopg2.connect(dbname='edge', host='0.0.0.0')#, password = 'password', user = 'postgres')
        self.cursor = self.conn.cursor()

    def create_table(self):
        command = """CREATE TABLE IF NOT EXISTS edge_messages (messages jsonb)"""
        self.cursor.execute(command)
        self.conn.commit()

    def close(self):
        self.conn.close()

    def insert(self, message):
        command = """INSERT INTO edge_messages VALUES (%s)"""
        data = json.dumps(message)
        self.cursor.execute(command, (data,))
        self.conn.commit()
        print('Inserted to DB')


# class DB_1:
#     def __init__(self):
#         self.conn = sqlite3.connect('cloud-events.db')
#         self.cursor = self.conn.cursor()
#
#     def create_table(self):
#         command = """CREATE TABLE IF NOT EXISTS edge_messages (
#             uuid text,
#             device_id text,
#             society_id text,
#             vsource text,
#             models text,
#             message text,
#             level text,
#             event text,
#             label text,
#             timestamp text,
#             url text
#         )"""
#         self.cursor.execute(command)
#         self.conn.commit()
#
#     def close(self):
#         self.conn.close()
#
#     def insert(self, message):
#         command = """INSERT INTO edge_messages VALUES ( :uuid, :device_id, :society_id ,:vsource, :models, :message, :level,
#         :event, :label, :timestamp, :url )"""
#         self.cursor.execute(command, message)
#         self.conn.commit()
#         print('Inserted to DB')

"""
conn = psycopg2.connect(dbname='edge', host='0.0.0.0', password = 'password', user = 'postgres')
cursor = conn.cursor()
cursor.execute("select * from edge_messages")
cursor.fetchall()

"""
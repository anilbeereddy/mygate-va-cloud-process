from utils.db_utils import DB
from utils.s3_utils import S3

import requests
import json

column_list = ['vsource', 'models', 'message', 'level', 'event',
               'label', 'timestamp', 'society_id', 'device_id', 'uuid', 'url', 'video_snippet']


trigger_url = 'http://0.0.0.0:8090/trigger'

class EventProcess:
    def __init__(self):
        self.database = DB()
        self.S3obj = S3()

    def save_to_db(self):
        self.database.insert(self.message)

    def upload_s3(self):
        self.request['url'] = self.S3obj.push(self.request['image'], self.request['uuid'])

    def trigger(self):
        response = requests.post(trigger_url, data=json.dumps(self.message),headers={'Content-type': 'application/json'})

    def extract(self):
        self.message = {}
        for k, v in self.request.items():
            if k in column_list:
                self.message[k] = v

        models = self.message['models']
        models_text = ''
        for m in models:
            models_text = models_text + m + ','
        self.message['models'] = models_text

    def process(self, request):
        self.request = request
        if request['level'] == 'alert':
            self.upload_s3()
        self.extract()
        self.save_to_db()
        self.trigger()

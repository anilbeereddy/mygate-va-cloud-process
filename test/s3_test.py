import boto3
import time
import cv2
import base64

"""
Boto Session
"""
bucket = 'mg-analytics-batch'
region = 'ap-south-1'
folder = "edge_evidence_images1"
file_name = "init_image_1.jpg"

url = f"https://{bucket}.s3.{region}.amazonaws.com/{folder}/{file_name}"

s3_location = 's3://mg-analytics-batch/edge_evidence_images',

session = boto3.Session(profile_name='data-pipeline')
s3 = session.resource('s3')
bucket = s3.Bucket(bucket)

image = cv2.imread('/Users/aravindsuresh/Downloads/static.png')
# image_string = base64.b64encode(cv2.imencode('.jpg', image)[1]).decode()

s = time.time()
image_string = cv2.imencode('.jpg', image)[1].tobytes()
print(time.time() - s)

s = time.time()

print(time.time() - s, ret)
